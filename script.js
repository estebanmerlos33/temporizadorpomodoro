var intervalo;
var segundos_minuto = 59;
var tiempo_trabajo = 25;
var tiempo_descanso = 5
var intervaloAccionado = false

var flag_trabajo = true;


var audio = new Audio('alarma.mp3');



function intervalo_reloj() {
    
    
    if(intervaloAccionado){
       
        return;}

    intervaloAccionado = true;
        
    document.getElementById("minuto").innerHTML = tiempo_trabajo;
    
   

    if(!flag_trabajo)
        cambiarAColorVerde();
    else{
        cambiarAColorRojo();
    }
    
    intervalo = setTimeout(descontar,1000)


}

function descontar(){
    let minutos = document.getElementById("minuto").innerHTML;
    let segundos = document.getElementById("segundo").innerHTML; 
    let segundosParse = parseInt(segundos);
    let minutosParse = parseInt(minutos);
    
    if(segundosParse>0)
        document.getElementById("segundo").innerHTML = segundosParse-1; 

    if(segundosParse===0){
        document.getElementById("segundo").innerHTML = segundos_minuto 
        document.getElementById("minuto").innerHTML = minutosParse-1;
    }

    if(minutosParse===0 && segundosParse===0){
        audio.play()
        
        reset_animation("campana-izquierda")
        reset_animation("campana-derecha")
        reset_animation("cuerpo")

        
        animacion_campana_izquierda()
        animacion_campana_derecha()
        animacion_reloj()
       

       
        flag_trabajo = !flag_trabajo;

        if(flag_trabajo){
            document.getElementById("minuto").innerHTML = tiempo_trabajo
            document.getElementById("segundo").innerHTML = 0
            cambiarAColorRojo();
        }
        else{
            document.getElementById("minuto").innerHTML = tiempo_descanso
            document.getElementById("segundo").innerHTML = 0;
            cambiarAColorVerde();
        }
    }


     intervalo = setTimeout(descontar,1000);
        
    

}




function terminar_intervalo() {
    intervaloAccionado = false;
    cambiarAColorRojo();
    document.getElementById("minuto").innerHTML = 0;
    document.getElementById("segundo").innerHTML = 0 + "0";
    clearInterval(intervalo);
    
}

function cambiarAColorVerde() {
    document.documentElement.style.setProperty('--color', 'green');
}
function cambiarAColorRojo() {

    document.documentElement.style.setProperty('--color', 'red');
}


function animacion_campana_izquierda() {

    document.getElementById("campana-izquierda").style.animationName= "vibracion";
    document.getElementById("campana-izquierda").style.animationDuration = "0.1s";
    document.getElementById("campana-izquierda").style.animationTimingFunction = "initial";
    document.getElementById("campana-izquierda").style.animationIterationCount = "15";

}

function reset_animation(elemento) {
    var el = document.getElementById(elemento);
    el.style.animation = 'none';
    el.offsetHeight; /* trigger reflow */
    el.style.animation = null; 
  }

function animacion_campana_derecha() {

    document.getElementById("campana-derecha").style.animationName= "vibracion2";
    document.getElementById("campana-derecha").style.animationDuration = "0.1s";
    document.getElementById("campana-derecha").style.animationTimingFunction = "initial";
    document.getElementById("campana-derecha").style.animationIterationCount = "15";

}
function animacion_reloj() {

    document.getElementById("cuerpo").style.animationName= "vibracion3";
    document.getElementById("cuerpo").style.animationDuration = "0.1s";
    document.getElementById("cuerpo").style.animationTimingFunction = "initial";
    document.getElementById("cuerpo").style.animationIterationCount = "15";
  
    

}


